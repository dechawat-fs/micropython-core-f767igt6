/*
 * ltdc.c
 *
 *  Created on: 29 September 2561
 *      Author: Dechawat Srisaiyud
 */
#include <string.h>
#include "py/mphal.h"
#include "py/nlr.h"
#include "py/obj.h"
#include "py/runtime.h"
#include "py/binary.h"
#include "py/stream.h"
#include "extmod/vfs.h"
#include "extmod/vfs_fat.h"
#include "pin.h"
#include "pin_static_af.h"
#include "fonts/fonts.h"

#if (MICROPY_HW_ENABLE_LTDC == 1)

#define ABS(X)  ((X) > 0 ? (X) : -(X))

typedef enum
{
	CENTER_MODE          = 0x01,    /* Center mode */
	RIGHT_MODE           = 0x02,    /* Right mode  */
	LEFT_MODE            = 0x03     /* Left mode   */
}align_mode_t;

typedef struct _ltdc_std_color_t
{
	const char *color_str;
	uint32_t argb8888;
} ltdc_std_color_t;

typedef struct _ltdc_pixel_format_table_t
{
	const char *format_string;
	uint32_t pixel_format;
	uint32_t require_fbsize;
	uint8_t bpp;
} ltdc_pixel_format_table_t;

typedef struct _ltdc_layer_info_t
{
	uint8_t init;
	uint8_t format_index;
	uint8_t visible;
	uint8_t alpha;
	uint32_t fbaddr;
	uint32_t fbsize;
} ltdc_layer_info_t;

typedef struct _ltdc_font_t
{
	const char *fontname;
	sFONT *font;
	void (*draw_char)(uint32_t layer, const struct _ltdc_font_t *f, uint32_t nobg, uint16_t X, uint16_t Y, const char Ascii, uint32_t textcolor, uint32_t bgcolor);
} ltdc_font_t;

// this is the actual C-structure for our new object
typedef struct _ltdc_drawprop_t
{
	// base represents some basic information, like type
	mp_obj_base_t base;

	uint8_t layer_index;
	uint32_t fgcolor;
	uint32_t bgcolor;
	const ltdc_font_t *font;
} ltdc_drawprop_t;

static ltdc_layer_info_t ltdc_layer_info[] = {
#if defined(MICROPY_LTDC_LAYER0_FB_ADDR) && defined(MICROPY_LTDC_LAYER0_FB_SIZE)
		{ 0, 0, 1, 255, MICROPY_LTDC_LAYER0_FB_ADDR, MICROPY_LTDC_LAYER0_FB_SIZE },
#else
		{ 0, 0, 1, 0, 0, 0 },
#endif
#if defined(MICROPY_LTDC_LAYER1_FB_ADDR) && defined(MICROPY_LTDC_LAYER1_FB_SIZE)
		{ 0, 0, 1, 255, MICROPY_LTDC_LAYER1_FB_ADDR, MICROPY_LTDC_LAYER1_FB_SIZE },
#else
		{ 0, 0, 1, 0, 0, 0 },
#endif
};
#define LTDC_MAX_LAYER_NUM		(sizeof(ltdc_layer_info)/sizeof(ltdc_layer_info_t))

#define PIXEL_FORMAT_ARGB8888_INDEX		0
#define PIXEL_FORMAT_RGB888_INDEX		1
#define PIXEL_FORMAT_RGB565_INDEX		2
static const ltdc_pixel_format_table_t pixel_format_table[] = {
		{ "ARGB8888", LTDC_PIXEL_FORMAT_ARGB8888, MICROPY_HW_LTDC_WIDTH*MICROPY_HW_LTDC_HEIGHT*4, 4 },	// 0: ARGB8888
		{ "RGB888", LTDC_PIXEL_FORMAT_RGB888, MICROPY_HW_LTDC_WIDTH*MICROPY_HW_LTDC_HEIGHT*3, 3 },		// 1: RGB888
		{ "RGB565", LTDC_PIXEL_FORMAT_RGB565, MICROPY_HW_LTDC_WIDTH*MICROPY_HW_LTDC_HEIGHT*2, 2 },		// 2: RGB565
};
#define PIXEL_FORMAT_NUM	(sizeof(pixel_format_table)/sizeof(ltdc_pixel_format_table_t))

static const ltdc_std_color_t std_color_table[] = {
		{"BLUE", 0xFF0000FF},
		{"GREEN", 0xFF00FF00},
		{"RED", 0xFFFF0000},
		{"CYAN", 0xFF00FFFF},
		{"MAGENTA", 0xFFFF00FF},
		{"YELLOW", 0xFFFFFF00},
		{"LIGHTBLUE", 0xFF8080FF},
		{"LIGHTGREEN", 0xFF80FF80},
		{"LIGHTRED", 0xFFFF8080},
		{"LIGHTCYAN", 0xFF80FFFF},
		{"LIGHTMAGENTA", 0xFFFF80FF},
		{"LIGHTYELLOW", 0xFFFFFF80},
		{"DARKBLUE", 0xFF000080},
		{"DARKGREEN", 0xFF008000},
		{"DARKRED", 0xFF800000},
		{"DARKCYAN", 0xFF008080},
		{"DARKMAGENTA", 0xFF800080},
		{"DARKYELLOW",  0xFF808000},
		{"WHITE", 0xFFFFFFFF},
		{"LIGHTGRAY", 0xFFD3D3D3},
		{"GRAY", 0xFF808080},
		{"DARKGRAY",0xFF404040},
		{"BLACK", 0xFF000000},
		{"BROWN", 0xFFA52A2A},
		{"ORANGE", 0xFFFFA500},
		{"TRANSPARENT",	0xFF000000},
};
#define STD_COLOR_NUM	(sizeof(std_color_table)/sizeof(ltdc_std_color_t))


static void ltdc_draw_char(uint32_t LayerIndex, const ltdc_font_t *font_in, uint32_t nobg, uint16_t Xpos, uint16_t Ypos, const char Ascii, uint32_t textcolor, uint32_t bgcolor);
static void ltdc_draw_char_2(uint32_t LayerIndex, const ltdc_font_t *font_in, uint32_t nobg, uint16_t x, uint16_t y, const char Ascii, uint32_t textcolor, uint32_t bgcolor);

static const ltdc_font_t std_font_table[] =
{
		{"Font24", &Font24, ltdc_draw_char},
		{"Font20", &Font20, ltdc_draw_char},
		{"Font16", &Font16, ltdc_draw_char},
		{"Font12", &Font12, ltdc_draw_char},
		{"Font8", &Font8, ltdc_draw_char},
		{"UbuntuMono24", &UbuntuMono24, ltdc_draw_char_2},
		{"UbuntuMono20", &UbuntuMono20, ltdc_draw_char_2},
		{"UbuntuMono16", &UbuntuMono16, ltdc_draw_char_2},
};
#define STD_FONT_NUM	(sizeof(std_font_table)/sizeof(ltdc_font_t))

/**
  * @brief LCD default font
  */
#define LCD_DEFAULT_FONT         (&std_font_table[0])

static LTDC_HandleTypeDef  ltdc_handle;
static DMA2D_HandleTypeDef dma2d_handle;

static void ltdc_gpio_init(void)
{
	/* Enable the LTDC and DMA2D clocks */
	__HAL_RCC_LTDC_CLK_ENABLE();
	__HAL_RCC_DMA2D_CLK_ENABLE();

	/*** LTDC Pins configuration ***/
	mp_hal_pin_config_alt_static(MICROPY_HW_LTDC_CLK, MP_HAL_PIN_MODE_ALT, MP_HAL_PIN_PULL_NONE, STATIC_AF_LCD_CLK);
	mp_hal_pin_config_alt_static(MICROPY_HW_LTDC_DE, MP_HAL_PIN_MODE_ALT, MP_HAL_PIN_PULL_NONE, STATIC_AF_LCD_DE);
	mp_hal_pin_config_alt_static(MICROPY_HW_LTDC_HSYNC, MP_HAL_PIN_MODE_ALT, MP_HAL_PIN_PULL_NONE, STATIC_AF_LCD_HSYNC);
	mp_hal_pin_config_alt_static(MICROPY_HW_LTDC_VSYNC, MP_HAL_PIN_MODE_ALT, MP_HAL_PIN_PULL_NONE, STATIC_AF_LCD_VSYNC);

	mp_hal_pin_config_alt_static(MICROPY_HW_LTDC_R7, MP_HAL_PIN_MODE_ALT, MP_HAL_PIN_PULL_NONE, STATIC_AF_LCD_R7);
	mp_hal_pin_config_alt_static(MICROPY_HW_LTDC_R6, MP_HAL_PIN_MODE_ALT, MP_HAL_PIN_PULL_NONE, STATIC_AF_LCD_R6);
	mp_hal_pin_config_alt_static(MICROPY_HW_LTDC_R5, MP_HAL_PIN_MODE_ALT, MP_HAL_PIN_PULL_NONE, STATIC_AF_LCD_R5);
	mp_hal_pin_config_alt_static(MICROPY_HW_LTDC_R4, MP_HAL_PIN_MODE_ALT, MP_HAL_PIN_PULL_NONE, STATIC_AF_LCD_R4);
	mp_hal_pin_config_alt_static(MICROPY_HW_LTDC_R3, MP_HAL_PIN_MODE_ALT, MP_HAL_PIN_PULL_NONE, STATIC_AF_LCD_R3);
	mp_hal_pin_config_alt_static(MICROPY_HW_LTDC_R2, MP_HAL_PIN_MODE_ALT, MP_HAL_PIN_PULL_NONE, STATIC_AF_LCD_R2);
	mp_hal_pin_config_alt_static(MICROPY_HW_LTDC_R1, MP_HAL_PIN_MODE_ALT, MP_HAL_PIN_PULL_NONE, STATIC_AF_LCD_R1);
	mp_hal_pin_config_alt_static(MICROPY_HW_LTDC_R0, MP_HAL_PIN_MODE_ALT, MP_HAL_PIN_PULL_NONE, STATIC_AF_LCD_R0);

	mp_hal_pin_config_alt_static(MICROPY_HW_LTDC_G7, MP_HAL_PIN_MODE_ALT, MP_HAL_PIN_PULL_NONE, STATIC_AF_LCD_G7);
	mp_hal_pin_config_alt_static(MICROPY_HW_LTDC_G6, MP_HAL_PIN_MODE_ALT, MP_HAL_PIN_PULL_NONE, STATIC_AF_LCD_G6);
	mp_hal_pin_config_alt_static(MICROPY_HW_LTDC_G5, MP_HAL_PIN_MODE_ALT, MP_HAL_PIN_PULL_NONE, STATIC_AF_LCD_G5);
	mp_hal_pin_config_alt_static(MICROPY_HW_LTDC_G4, MP_HAL_PIN_MODE_ALT, MP_HAL_PIN_PULL_NONE, STATIC_AF_LCD_G4);
	mp_hal_pin_config_alt_static(MICROPY_HW_LTDC_G3, MP_HAL_PIN_MODE_ALT, MP_HAL_PIN_PULL_NONE, STATIC_AF_LCD_G3);
	mp_hal_pin_config_alt_static(MICROPY_HW_LTDC_G2, MP_HAL_PIN_MODE_ALT, MP_HAL_PIN_PULL_NONE, STATIC_AF_LCD_G2);
	mp_hal_pin_config_alt_static(MICROPY_HW_LTDC_G1, MP_HAL_PIN_MODE_ALT, MP_HAL_PIN_PULL_NONE, STATIC_AF_LCD_G1);
	mp_hal_pin_config_alt_static(MICROPY_HW_LTDC_G0, MP_HAL_PIN_MODE_ALT, MP_HAL_PIN_PULL_NONE, STATIC_AF_LCD_G0);

	mp_hal_pin_config_alt_static(MICROPY_HW_LTDC_B7, MP_HAL_PIN_MODE_ALT, MP_HAL_PIN_PULL_NONE, STATIC_AF_LCD_B7);
	mp_hal_pin_config_alt_static(MICROPY_HW_LTDC_B6, MP_HAL_PIN_MODE_ALT, MP_HAL_PIN_PULL_NONE, STATIC_AF_LCD_B6);
	mp_hal_pin_config_alt_static(MICROPY_HW_LTDC_B5, MP_HAL_PIN_MODE_ALT, MP_HAL_PIN_PULL_NONE, STATIC_AF_LCD_B5);
	mp_hal_pin_config_alt_static(MICROPY_HW_LTDC_B4, MP_HAL_PIN_MODE_ALT, MP_HAL_PIN_PULL_NONE, STATIC_AF_LCD_B4);
	mp_hal_pin_config_alt_static(MICROPY_HW_LTDC_B3, MP_HAL_PIN_MODE_ALT, MP_HAL_PIN_PULL_NONE, STATIC_AF_LCD_B3);
	mp_hal_pin_config_alt_static(MICROPY_HW_LTDC_B2, MP_HAL_PIN_MODE_ALT, MP_HAL_PIN_PULL_NONE, STATIC_AF_LCD_B2);
	mp_hal_pin_config_alt_static(MICROPY_HW_LTDC_B1, MP_HAL_PIN_MODE_ALT, MP_HAL_PIN_PULL_NONE, STATIC_AF_LCD_B1);
	mp_hal_pin_config_alt_static(MICROPY_HW_LTDC_B0, MP_HAL_PIN_MODE_ALT, MP_HAL_PIN_PULL_NONE, STATIC_AF_LCD_B0);

#if defined(MICROPY_HW_LCD_BL)
	mp_hal_pin_output(MICROPY_HW_LCD_BL);

	/* Backlight default high */
	mp_hal_pin_high(MICROPY_HW_LCD_BL);
#endif
}

static void lcd_backlight(int state)
{
#if defined(MICROPY_HW_LCD_BL)
	if(state)
		mp_hal_pin_high(MICROPY_HW_LCD_BL);
	else
		mp_hal_pin_low(MICROPY_HW_LCD_BL);
#endif
}

static void ltdc_init(void)
{
	/* Timing Configuration */
	ltdc_handle.Init.HorizontalSync = (MICROPY_LTDC_TIMING_HSYNC - 1);
	ltdc_handle.Init.VerticalSync = (MICROPY_LTDC_TIMING_VSYNC - 1);
	ltdc_handle.Init.AccumulatedHBP = (MICROPY_LTDC_TIMING_HSYNC + MICROPY_LTDC_TIMING_HBP - 1);
	ltdc_handle.Init.AccumulatedVBP = (MICROPY_LTDC_TIMING_VSYNC + MICROPY_LTDC_TIMING_VBP - 1);
	ltdc_handle.Init.AccumulatedActiveH = (MICROPY_HW_LTDC_HEIGHT + MICROPY_LTDC_TIMING_VSYNC + MICROPY_LTDC_TIMING_VBP - 1);
	ltdc_handle.Init.AccumulatedActiveW = (MICROPY_HW_LTDC_WIDTH + MICROPY_LTDC_TIMING_HSYNC + MICROPY_LTDC_TIMING_HBP - 1);
	ltdc_handle.Init.TotalHeigh = (MICROPY_HW_LTDC_HEIGHT + MICROPY_LTDC_TIMING_VSYNC + MICROPY_LTDC_TIMING_VBP + MICROPY_LTDC_TIMING_VFP - 1);
	ltdc_handle.Init.TotalWidth = (MICROPY_HW_LTDC_WIDTH + MICROPY_LTDC_TIMING_HSYNC + MICROPY_LTDC_TIMING_HBP + MICROPY_LTDC_TIMING_HFP - 1);

	/* Initialize the LCD pixel width and pixel height */
	ltdc_handle.LayerCfg->ImageWidth  = MICROPY_HW_LTDC_WIDTH;
	ltdc_handle.LayerCfg->ImageHeight = MICROPY_HW_LTDC_HEIGHT;

	/* Background value */
	ltdc_handle.Init.Backcolor.Blue = 0;
	ltdc_handle.Init.Backcolor.Green = 0;
	ltdc_handle.Init.Backcolor.Red = 0;

	/* Polarity */
	ltdc_handle.Init.HSPolarity = MICROPY_LTDC_HSPOLARITY;
	ltdc_handle.Init.VSPolarity = MICROPY_LTDC_VSPOLARITY;
	ltdc_handle.Init.DEPolarity = MICROPY_LTDC_DEPOLARITY;
	ltdc_handle.Init.PCPolarity = MICROPY_LTDC_PCPOLARITY;
	ltdc_handle.Instance = LTDC;

	ltdc_gpio_init();

	HAL_LTDC_Init(&ltdc_handle);
}

/**
  * @brief  Enables the display.
  * @retval None
  */
static void ltdc_enable(void)
{
  /* Display On */
  __HAL_LTDC_ENABLE(&ltdc_handle);
}

/**
  * @brief  Disables the display.
  * @retval None
  */
static void ltdc_disable(void)
{
  /* Display Off */
  __HAL_LTDC_DISABLE(&ltdc_handle);
}

/**
  * @brief  Initializes the LCD layers.
  * @param  LayerIndex: Layer foreground or background
  * @param  FB_Address: Layer frame buffer
  * @retval None
  */
static void ltdc_layer_init(int layer_inx, int format_inx)
{
	LTDC_LayerCfgTypeDef  layer_cfg;

	/* Layer Init */
	layer_cfg.WindowX0 = 0;
	layer_cfg.WindowX1 = MICROPY_HW_LTDC_WIDTH;
	layer_cfg.WindowY0 = 0;
	layer_cfg.WindowY1 = MICROPY_HW_LTDC_HEIGHT;
	layer_cfg.PixelFormat = pixel_format_table[format_inx].pixel_format;
	layer_cfg.FBStartAdress = ltdc_layer_info[layer_inx].fbaddr;
	layer_cfg.Alpha = ltdc_layer_info[layer_inx].alpha;
	layer_cfg.Alpha0 = 0;
	layer_cfg.Backcolor.Blue = 0;
	layer_cfg.Backcolor.Green = 0;
	layer_cfg.Backcolor.Red = 0;
	layer_cfg.BlendingFactor1 = LTDC_BLENDING_FACTOR1_PAxCA;
	layer_cfg.BlendingFactor2 = LTDC_BLENDING_FACTOR2_PAxCA;
	layer_cfg.ImageWidth = MICROPY_HW_LTDC_WIDTH;
	layer_cfg.ImageHeight = MICROPY_HW_LTDC_HEIGHT;

	HAL_LTDC_ConfigLayer(&ltdc_handle, &layer_cfg, layer_inx);
}

/**
  * @brief  Sets an LCD Layer visible
  * @param  LayerIndex: Visible Layer
  * @param  State: New state of the specified layer
  *          This parameter can be one of the following values:
  *            @arg  ENABLE
  *            @arg  DISABLE
  * @retval None
  */
static void ltdc_layer_visible(uint32_t LayerIndex, FunctionalState State)
{
  if(State == ENABLE)
  {
    __HAL_LTDC_LAYER_ENABLE(&ltdc_handle, LayerIndex);
  }
  else
  {
    __HAL_LTDC_LAYER_DISABLE(&ltdc_handle, LayerIndex);
  }
  __HAL_LTDC_RELOAD_CONFIG(&ltdc_handle);
}

/**
  * @brief  Configures the transparency.
  * @param  LayerIndex: Layer foreground or background.
  * @param  Transparency: Transparency
  *           This parameter must be a number between Min_Data = 0x00 and Max_Data = 0xFF
  * @retval None
  */
void ltdc_layer_alpha(uint32_t LayerIndex, uint8_t Transparency)
{
  HAL_LTDC_SetAlpha(&ltdc_handle, Transparency, LayerIndex);
}


/**
  * @brief  Draws a pixel on LCD.
  * @param  Xpos: X position
  * @param  Ypos: Y position
  * @param  RGB_Code: Pixel color in ARGB mode (8-8-8-8)
  * @retval None
  */
static void ltdc_draw_pixel(uint32_t LayerIndex, uint32_t Xpos, uint32_t Ypos, uint32_t RGB_Code)
{
	/* Write data value to all SDRAM memory */
	if(Xpos < MICROPY_HW_LTDC_WIDTH && Ypos < MICROPY_HW_LTDC_HEIGHT)
	{
		if(ltdc_layer_info[LayerIndex].format_index == PIXEL_FORMAT_ARGB8888_INDEX)
		{
			*(__IO uint32_t*) (ltdc_handle.LayerCfg[LayerIndex].FBStartAdress + (4*(Ypos*MICROPY_HW_LTDC_WIDTH + Xpos))) = RGB_Code;
		}
		else if(ltdc_layer_info[LayerIndex].format_index == PIXEL_FORMAT_RGB888_INDEX)
		{
			*(__IO uint8_t*) (ltdc_handle.LayerCfg[LayerIndex].FBStartAdress + (3*(Ypos*MICROPY_HW_LTDC_WIDTH + Xpos))) = (RGB_Code >> 16) & 0xFF;
			*(__IO uint8_t*) (ltdc_handle.LayerCfg[LayerIndex].FBStartAdress + (3*(Ypos*MICROPY_HW_LTDC_WIDTH + Xpos)) + 1) = (RGB_Code >> 8) & 0xFF;
			*(__IO uint8_t*) (ltdc_handle.LayerCfg[LayerIndex].FBStartAdress + (3*(Ypos*MICROPY_HW_LTDC_WIDTH + Xpos)) + 2) = (RGB_Code) & 0xFF;
		}
		else
		{
			*(__IO uint16_t*) (ltdc_handle.LayerCfg[LayerIndex].FBStartAdress + (4*(Ypos*MICROPY_HW_LTDC_WIDTH + Xpos))) = (uint16_t)(((RGB_Code & 0x00F80000) >> 8) |
																															((RGB_Code & 0x0000FC00) >> 5) |
																															((RGB_Code & 0x000000F8) >> 3));
		}
	}
}

/**
  * @brief  Fills a buffer.
  * @param  LayerIndex: Layer index
  * @param  pDst: Pointer to destination buffer
  * @param  xSize: Buffer width
  * @param  ySize: Buffer height
  * @param  OffLine: Offset
  * @param  ColorIndex: Color index
  * @retval None
  */
static void LL_FillBuffer(uint32_t LayerIndex, uint32_t Xpos, uint32_t Ypos, uint32_t Xsize, uint32_t Ysize, uint32_t OffLine, uint32_t RGB_Code)
{
	uint32_t fbaddr;
	uint8_t bpp;

	/* Register to memory mode with ARGB8888 as color Mode */
	dma2d_handle.Init.Mode         = DMA2D_R2M;

	if(ltdc_layer_info[LayerIndex].format_index == PIXEL_FORMAT_ARGB8888_INDEX)
	{
		dma2d_handle.Init.ColorMode    = DMA2D_OUTPUT_ARGB8888;
		bpp = 4;
	}
	else if(ltdc_layer_info[LayerIndex].format_index == PIXEL_FORMAT_RGB888_INDEX)
	{
		dma2d_handle.Init.ColorMode    = DMA2D_OUTPUT_RGB888;
		bpp = 3;
	}
	else
	{
		dma2d_handle.Init.ColorMode    = DMA2D_OUTPUT_RGB565;
		bpp = 2;
	}

	dma2d_handle.Init.OutputOffset = OffLine;
	dma2d_handle.Instance = DMA2D;

	/* Foreground Configuration */
	dma2d_handle.LayerCfg[1].AlphaMode = DMA2D_NO_MODIF_ALPHA;
	dma2d_handle.LayerCfg[1].InputAlpha = 0xFF;
	dma2d_handle.LayerCfg[1].InputColorMode = DMA2D_INPUT_ARGB8888;
	dma2d_handle.LayerCfg[1].InputOffset = 0;


	fbaddr = (uint32_t)ltdc_layer_info[LayerIndex].fbaddr + (((uint32_t)Ypos*MICROPY_HW_LTDC_WIDTH + Xpos)*bpp);

	if(Xpos + Xsize > MICROPY_HW_LTDC_WIDTH)
		Xsize = MICROPY_HW_LTDC_WIDTH - Xpos;

	if(Ypos + Ysize > MICROPY_HW_LTDC_HEIGHT)
		Ysize = MICROPY_HW_LTDC_HEIGHT - Ypos;

	/* DMA2D Initialization */
	if(HAL_DMA2D_Init(&dma2d_handle) == HAL_OK)
	{
		if(HAL_DMA2D_ConfigLayer(&dma2d_handle, LayerIndex) == HAL_OK)
		{
			if (HAL_DMA2D_Start(&dma2d_handle, RGB_Code, fbaddr, Xsize, Ysize) == HAL_OK)
			{
				/* Polling For DMA transfer */
				HAL_DMA2D_PollForTransfer(&dma2d_handle, 10);
			}
		}
	}
}

/**
  * @brief  Draws an horizontal line.
  * @param  Xpos: X position
  * @param  Ypos: Y position
  * @param  Length: Line length
  * @retval None
  */
static void ltdc_draw_hline(uint32_t LayerIndex, uint32_t Xpos, uint32_t Ypos, uint32_t Length, uint32_t RGB_Code)
{
	/* Write line */
	LL_FillBuffer(LayerIndex, Xpos, Ypos, Length, 1, 0, RGB_Code);
}

/**
  * @brief  Draws a vertical line.
  * @param  Xpos: X position
  * @param  Ypos: Y position
  * @param  Length: Line length
  * @retval None
  */
static void ltdc_draw_vline(uint32_t LayerIndex, uint32_t Xpos, uint32_t Ypos, uint32_t Length, uint32_t RGB_Code)
{
	LL_FillBuffer(LayerIndex, Xpos, Ypos, 1, Length, MICROPY_HW_LTDC_WIDTH - 1, RGB_Code);
}

/**
  * @brief  Draws an uni-line (between two points).
  * @param  x1: Point 1 X position
  * @param  y1: Point 1 Y position
  * @param  x2: Point 2 X position
  * @param  y2: Point 2 Y position
  * @retval None
  */
static void ltdc_draw_line(uint32_t LayerIndex, uint32_t x1, uint32_t y1, uint32_t x2, uint32_t y2, uint32_t RGB_Code)
{
	int32_t deltax = 0, deltay = 0, x = 0, y = 0, xinc1 = 0, xinc2 = 0,
	yinc1 = 0, yinc2 = 0, den = 0, num = 0, num_add = 0, num_pixels = 0,
	curpixel = 0;

	deltax = ABS(((int32_t)x2) - ((int32_t)x1));        /* The difference between the x's */
	deltay = ABS(((int32_t)y2) - ((int32_t)y1));        /* The difference between the y's */
	x = x1;                       /* Start x off at the first pixel */
	y = y1;                       /* Start y off at the first pixel */

	if (x2 >= x1)                 /* The x-values are increasing */
	{
		xinc1 = 1;
		xinc2 = 1;
	}
	else                          /* The x-values are decreasing */
	{
		xinc1 = -1;
		xinc2 = -1;
	}

	if (y2 >= y1)                 /* The y-values are increasing */
	{
		yinc1 = 1;
		yinc2 = 1;
	}
	else                          /* The y-values are decreasing */
	{
		yinc1 = -1;
		yinc2 = -1;
	}

	if (deltax >= deltay)         /* There is at least one x-value for every y-value */
	{
		xinc1 = 0;                  /* Don't change the x when numerator >= denominator */
		yinc2 = 0;                  /* Don't change the y for every iteration */
		den = deltax;
		num = deltax / 2;
		num_add = deltay;
		num_pixels = deltax;         /* There are more x-values than y-values */
	}
	else                          /* There is at least one y-value for every x-value */
	{
		xinc2 = 0;                  /* Don't change the x for every iteration */
		yinc1 = 0;                  /* Don't change the y when numerator >= denominator */
		den = deltay;
		num = deltay / 2;
		num_add = deltax;
		num_pixels = deltay;         /* There are more y-values than x-values */
	}

	for (curpixel = 0; curpixel <= num_pixels; curpixel++)
	{
		ltdc_draw_pixel(LayerIndex, x, y, RGB_Code);   /* Draw the current pixel */
		num += num_add;                            /* Increase the numerator by the top of the fraction */
		if (num >= den)                           /* Check if numerator >= denominator */
		{
			num -= den;                             /* Calculate the new numerator value */
			x += xinc1;                             /* Change the x as appropriate */
			y += yinc1;                             /* Change the y as appropriate */
		}
		x += xinc2;                               /* Change the x as appropriate */
		y += yinc2;                               /* Change the y as appropriate */
	}
}

/**
  * @brief  Draws a rectangle.
  * @param  Xpos: X position
  * @param  Ypos: Y position
  * @param  Width: Rectangle width
  * @param  Height: Rectangle height
  * @retval None
  */
static void ltdc_draw_rect(uint32_t LayerIndex, uint32_t Xpos, uint32_t Ypos, uint32_t Width, uint32_t Height, uint32_t RGB_Code)
{
	/* Draw horizontal lines */
	ltdc_draw_hline(LayerIndex, Xpos, Ypos, Width, RGB_Code);
	ltdc_draw_hline(LayerIndex, Xpos, (Ypos+ Height), Width, RGB_Code);

	/* Draw vertical lines */
	ltdc_draw_vline(LayerIndex, Xpos, Ypos, Height, RGB_Code);
	ltdc_draw_vline(LayerIndex, (Xpos + Width), Ypos, Height, RGB_Code);
}

/**
  * @brief  Draws a circle.
  * @param  Xpos: X position
  * @param  Ypos: Y position
  * @param  Radius: Circle radius
  * @retval None
  */
static void ltdc_draw_circle(uint32_t LayerIndex, uint32_t Xpos, uint32_t Ypos, uint32_t Radius, uint32_t RGB_Code)
{
  int32_t   decision;    /* Decision Variable */
  uint32_t  current_x;   /* Current X Value */
  uint32_t  current_y;   /* Current Y Value */

  decision = 3 - (Radius << 1);
  current_x = 0;
  current_y = Radius;

  while (current_x <= current_y)
  {
	  ltdc_draw_pixel(LayerIndex, (Xpos + current_x), (Ypos - current_y), RGB_Code);

	  ltdc_draw_pixel(LayerIndex, (Xpos - current_x), (Ypos - current_y), RGB_Code);

	  ltdc_draw_pixel(LayerIndex, (Xpos + current_y), (Ypos - current_x), RGB_Code);

	  ltdc_draw_pixel(LayerIndex, (Xpos - current_y), (Ypos - current_x), RGB_Code);

	  ltdc_draw_pixel(LayerIndex, (Xpos + current_x), (Ypos + current_y), RGB_Code);

	  ltdc_draw_pixel(LayerIndex, (Xpos - current_x), (Ypos + current_y), RGB_Code);

	  ltdc_draw_pixel(LayerIndex, (Xpos + current_y), (Ypos + current_x), RGB_Code);

	  ltdc_draw_pixel(LayerIndex, (Xpos - current_y), (Ypos + current_x), RGB_Code);

    if (decision < 0)
    {
      decision += (current_x << 2) + 6;
    }
    else
    {
      decision += ((current_x - current_y) << 2) + 10;
      current_y--;
    }
    current_x++;
  }
}

/**
  * @brief  Draws an ellipse on LCD.
  * @param  Xpos: X position
  * @param  Ypos: Y position
  * @param  XRadius: Ellipse X radius
  * @param  YRadius: Ellipse Y radius
  * @retval None
  */
static void ltdc_draw_ellipse(uint32_t LayerIndex, int Xpos, int Ypos, int XRadius, int YRadius, uint32_t RGB_Code)
{
	int x = 0, y = -YRadius, err = 2-2*XRadius, e2;
	float k = 0, rad1 = 0, rad2 = 0;

	rad1 = XRadius;
	rad2 = YRadius;

	k = (float)(rad2/rad1);

	do {
		ltdc_draw_pixel(LayerIndex, (Xpos-(uint16_t)(x/k)), (Ypos+y), RGB_Code);
		ltdc_draw_pixel(LayerIndex, (Xpos+(uint16_t)(x/k)), (Ypos+y), RGB_Code);
		ltdc_draw_pixel(LayerIndex, (Xpos+(uint16_t)(x/k)), (Ypos-y), RGB_Code);
		ltdc_draw_pixel(LayerIndex, (Xpos-(uint16_t)(x/k)), (Ypos-y), RGB_Code);

		e2 = err;
		if (e2 <= x) {
			err += ++x*2+1;
			if (-y == x && e2 <= y) e2 = 0;
		}
		if (e2 > y) err += ++y*2+1;
	}
	while (y <= 0);
}

/**
  * @brief  Draws a full rectangle.
  * @param  Xpos: X position
  * @param  Ypos: Y position
  * @param  Width: Rectangle width
  * @param  Height: Rectangle height
  * @retval None
  */
static void ltdc_draw_fillrect(uint32_t LayerIndex, uint32_t Xpos, uint32_t Ypos, uint32_t Width, uint32_t Height, uint32_t RGB_Code)
{
  /* Fill the rectangle */
  LL_FillBuffer(LayerIndex, Xpos, Ypos, Width, Height, MICROPY_HW_LTDC_WIDTH - Width, RGB_Code);
}

/**
  * @brief  Draws a full circle.
  * @param  Xpos: X position
  * @param  Ypos: Y position
  * @param  Radius: Circle radius
  * @retval None
  */
static void ltdc_draw_fillcircle(uint32_t LayerIndex, uint32_t Xpos, uint32_t Ypos, uint32_t Radius, uint32_t RGB_Code)
{
	int32_t  decision;     /* Decision Variable */
	uint32_t  current_x;   /* Current X Value */
	uint32_t  current_y;   /* Current Y Value */

	decision = 3 - (Radius << 1);

	current_x = 0;
	current_y = Radius;

	while (current_x <= current_y)
	{
		if(current_y > 0)
		{
			ltdc_draw_hline(LayerIndex, Xpos - current_y, Ypos + current_x, 2*current_y, RGB_Code);
			ltdc_draw_hline(LayerIndex, Xpos - current_y, Ypos - current_x, 2*current_y, RGB_Code);
		}

		if(current_x > 0)
		{
			ltdc_draw_hline(LayerIndex, Xpos - current_x, Ypos - current_y, 2*current_x, RGB_Code);
			ltdc_draw_hline(LayerIndex, Xpos - current_x, Ypos + current_y, 2*current_x, RGB_Code);
		}
		if (decision < 0)
		{
			decision += (current_x << 2) + 6;
		}
		else
		{
			decision += ((current_x - current_y) << 2) + 10;
			current_y--;
		}
		current_x++;
	}

	ltdc_draw_circle(LayerIndex, Xpos, Ypos, Radius, RGB_Code);
}


/**
  * @brief  Draws a full ellipse.
  * @param  Xpos: X position
  * @param  Ypos: Y position
  * @param  XRadius: Ellipse X radius
  * @param  YRadius: Ellipse Y radius
  * @retval None
  */
static void ltdc_draw_fillellipse(uint32_t LayerIndex, int Xpos, int Ypos, int XRadius, int YRadius, uint32_t RGB_Code)
{
	int x = 0, y = -YRadius, err = 2-2*XRadius, e2;
	float k = 0, rad1 = 0, rad2 = 0;

	rad1 = XRadius;
	rad2 = YRadius;

	k = (float)(rad2/rad1);

	do
	{
		ltdc_draw_hline(LayerIndex, (Xpos-(uint16_t)(x/k)), (Ypos+y), (2*(uint16_t)(x/k) + 1), RGB_Code);
		ltdc_draw_hline(LayerIndex, (Xpos-(uint16_t)(x/k)), (Ypos-y), (2*(uint16_t)(x/k) + 1), RGB_Code);

		e2 = err;
		if (e2 <= x)
		{
			err += ++x*2+1;
			if (-y == x && e2 <= y) e2 = 0;
		}
		if (e2 > y) err += ++y*2+1;
	}
	while (y <= 0);
}

/**
  * @brief  Fills a triangle (between 3 points).
  * @param  x1: Point 1 X position
  * @param  y1: Point 1 Y position
  * @param  x2: Point 2 X position
  * @param  y2: Point 2 Y position
  * @param  x3: Point 3 X position
  * @param  y3: Point 3 Y position
  * @retval None
  */
static void FillTriangle(uint32_t LayerIndex, uint16_t x1, uint16_t x2, uint16_t x3, uint16_t y1, uint16_t y2, uint16_t y3, uint32_t RGB_Code)
{
  int16_t deltax = 0, deltay = 0, x = 0, y = 0, xinc1 = 0, xinc2 = 0,
  yinc1 = 0, yinc2 = 0, den = 0, num = 0, num_add = 0, num_pixels = 0,
  curpixel = 0;

  deltax = ABS(x2 - x1);        /* The difference between the x's */
  deltay = ABS(y2 - y1);        /* The difference between the y's */
  x = x1;                       /* Start x off at the first pixel */
  y = y1;                       /* Start y off at the first pixel */

  if (x2 >= x1)                 /* The x-values are increasing */
  {
    xinc1 = 1;
    xinc2 = 1;
  }
  else                          /* The x-values are decreasing */
  {
    xinc1 = -1;
    xinc2 = -1;
  }

  if (y2 >= y1)                 /* The y-values are increasing */
  {
    yinc1 = 1;
    yinc2 = 1;
  }
  else                          /* The y-values are decreasing */
  {
    yinc1 = -1;
    yinc2 = -1;
  }

  if (deltax >= deltay)         /* There is at least one x-value for every y-value */
  {
    xinc1 = 0;                  /* Don't change the x when numerator >= denominator */
    yinc2 = 0;                  /* Don't change the y for every iteration */
    den = deltax;
    num = deltax / 2;
    num_add = deltay;
    num_pixels = deltax;         /* There are more x-values than y-values */
  }
  else                          /* There is at least one y-value for every x-value */
  {
    xinc2 = 0;                  /* Don't change the x for every iteration */
    yinc1 = 0;                  /* Don't change the y when numerator >= denominator */
    den = deltay;
    num = deltay / 2;
    num_add = deltax;
    num_pixels = deltay;         /* There are more y-values than x-values */
  }

  for (curpixel = 0; curpixel <= num_pixels; curpixel++)
  {
    ltdc_draw_line(LayerIndex, x, y, x3, y3, RGB_Code);

    num += num_add;              /* Increase the numerator by the top of the fraction */
    if (num >= den)             /* Check if numerator >= denominator */
    {
      num -= den;               /* Calculate the new numerator value */
      x += xinc1;               /* Change the x as appropriate */
      y += yinc1;               /* Change the y as appropriate */
    }
    x += xinc2;                 /* Change the x as appropriate */
    y += yinc2;                 /* Change the y as appropriate */
  }
}

/**
  * @brief  Draws a character on LCD.
  * @param  Xpos: Line where to display the character shape
  * @param  Ypos: Start column address
  * @param  c: Pointer to the character data
  * @retval None
  */
static void ltdc_draw_char(uint32_t LayerIndex, const ltdc_font_t *font_in, uint32_t nobg, uint16_t Xpos, uint16_t Ypos, const char Ascii, uint32_t textcolor, uint32_t bgcolor)
{
	uint32_t i = 0, j = 0;
	uint8_t  offset;
	uint8_t  *pchar;
	uint32_t line;
	const uint8_t *c;
	sFONT *font = (sFONT*)font_in->font;

	c = &font->table[(Ascii-' ') * font->Height * ((font->Width + 7) / 8)];

	offset =  8 *((font->Width + 7)/8) -  font->Width ;

	for(i = 0; i < font->Height; i++)
	{
		pchar = ((uint8_t*)c + (font->Width + 7)/8 * i);

		switch(((font->Width + 7)/8))
		{

		case 1:
			line =  pchar[0];
			break;

		case 2:
			line =  (pchar[0]<< 8) | pchar[1];
			break;

		case 3:
		default:
			line =  (pchar[0]<< 16) | (pchar[1]<< 8) | pchar[2];
			break;
		}

		for (j = 0; j < font->Width; j++)
		{
			if(line & (1 << (font->Width- j + offset- 1)))
			{
				ltdc_draw_pixel(LayerIndex, (Xpos + j), Ypos, textcolor);
			}
      		else
      		{
    			if(!nobg)
    				ltdc_draw_pixel(LayerIndex, (Xpos + j), Ypos, bgcolor);
      		}
		}
		Ypos++;
	}
}


/**
  * @brief  Draws a character on LCD.
  * @param  fcs: pointer to the fconsole handle object.
  * @param  Xpos: Line where to display the character shape
  * @param  Ypos: Start column address
  * @param  c: Pointer to the character data
  * @retval None
  */
static void ltdc_draw_char_2(uint32_t LayerIndex, const ltdc_font_t *font_in, uint32_t nobg, uint16_t x, uint16_t y, const char Ascii, uint32_t textcolor, uint32_t bgcolor)
{
	uint32_t i = 0, j = 0;
	uint16_t height, width;
  uint32_t line;
  const uint8_t *data;
  const GUI_CHARINFO *table = (const GUI_CHARINFO*)font_in->font->table;
  const uint8_t c = (const uint8_t)Ascii;

  data = table[c - 0x20].pData;
  height = font_in->font->Height;
  width  = table[c - 0x20].XSize;

  for(i = 0; i < height; i++)
  {
    switch(table[c - 0x20].BytesPerLine)
    {
    case 1:
      line = (*data++) << 24;
      break;

    case 2:
      line = data[0] << 24 | data[1] << 16;
      data += 2;
      break;

    case 3:
    default:
      line = (data[0] << 24) | (data[1]<< 16) | data[2] << 8;
      data += 3;
      break;
    }

    for (j = 0; j < width; j++)
    {
      if(line & (0x80000000 >> j))
      {
    	  ltdc_draw_pixel(LayerIndex, (x + j), y, textcolor);
      }
      else
      {
    	  ltdc_draw_pixel(LayerIndex, (x + j), y, bgcolor);
      }
    }
    y++;
  }
}

/**
  * @brief  Displays characters on the LCD.
  * @param  Xpos: X position (in pixel)
  * @param  Ypos: Y position (in pixel)
  * @param  Text: Pointer to string to display on LCD
  * @param  Mode: Display mode
  *          This parameter can be one of the following values:
  *            @arg  CENTER_MODE
  *            @arg  RIGHT_MODE
  *            @arg  LEFT_MODE
  * @retval None
  */
void ltdc_draw_string(uint32_t LayerIndex, const ltdc_font_t *font_in, uint32_t nobg, uint16_t Xpos, uint16_t Ypos, const char *Text, align_mode_t Mode, uint32_t textcolor, uint32_t bgcolor)
{
  uint16_t ref_column = 1, i = 0;
  uint32_t size = 0, xsize = 0;
  const char *ptr = Text;
  sFONT *font = (sFONT*)font_in->font;

  /* Get the text size */
  while (*ptr++) size ++ ;

  /* Characters number per line */
  xsize = (MICROPY_HW_LTDC_WIDTH/font->Width);

  switch (Mode)
  {
  case CENTER_MODE:
    {
      ref_column = Xpos + ((xsize - size)* font->Width) / 2;
      break;
    }
  case LEFT_MODE:
    {
      ref_column = Xpos;
      break;
    }
  case RIGHT_MODE:
    {
      ref_column = - Xpos + ((xsize - size)*font->Width);
      break;
    }
  default:
    {
      ref_column = Xpos;
      break;
    }
  }

  /* Check that the Start column is located in the screen */
  if ((ref_column < 1) || (ref_column >= 0x8000))
  {
    ref_column = 1;
  }

  /* Send the string character by character on LCD */
  while ((*Text != 0) & (((MICROPY_HW_LTDC_WIDTH - (i*font->Width)) & 0xFFFF) >= font->Width))
  {
    /* Display one character on LCD */
    font_in->draw_char(LayerIndex, font_in, nobg, ref_column, Ypos, *Text, textcolor, bgcolor);
    /* Decrement the column position by 16 */
    ref_column += font->Width;
    /* Point on the next character */
    Text++;
    i++;
  }
}

/**
  * @brief  Converts a line to an ARGB8888 pixel format.
  * @param  pSrc: Pointer to source buffer
  * @param  pDst: Output color
  * @param  xSize: Buffer width
  * @param  ColorMode: Input color mode
  * @retval None
  */
static void LL_ConvertLineFormat(void *pSrc, void *pDst, uint32_t xSize, uint32_t InputColorMode, uint32_t OutputColorMode)
{
	/* Configure the DMA2D Mode, Color Mode and output offset */
	dma2d_handle.Init.Mode         = DMA2D_M2M_PFC;
	dma2d_handle.Init.ColorMode    = OutputColorMode;
	dma2d_handle.Init.OutputOffset = 0;

	/* Foreground Configuration */
	dma2d_handle.LayerCfg[1].AlphaMode = DMA2D_NO_MODIF_ALPHA;
	dma2d_handle.LayerCfg[1].InputAlpha = 0xFF;
	dma2d_handle.LayerCfg[1].InputColorMode = InputColorMode;
	dma2d_handle.LayerCfg[1].InputOffset = 0;

	dma2d_handle.Instance = DMA2D;

	/* DMA2D Initialization */
	if(HAL_DMA2D_Init(&dma2d_handle) == HAL_OK)
	{
		if(HAL_DMA2D_ConfigLayer(&dma2d_handle, 1) == HAL_OK)
		{
			if (HAL_DMA2D_Start(&dma2d_handle, (uint32_t)pSrc, (uint32_t)pDst, xSize, 1) == HAL_OK)
			{
				/* Polling For DMA transfer */
				HAL_DMA2D_PollForTransfer(&dma2d_handle, 10);
			}
		}
	}
}


/**
  * @brief  Draws a bitmap picture loaded in the internal Flash (32 bpp).
  * @param  Xpos: Bmp X position in the LCD
  * @param  Ypos: Bmp Y position in the LCD
  * @param  pbmp: Pointer to Bmp picture address in the internal Flash
  * @retval None
  */
static void ltdc_draw_bitmap(uint32_t LayerIndex, uint32_t Xpos, uint32_t Ypos, const uint8_t *pbmp)
{
  uint32_t index = 0, width = 0, height = 0, bit_pixel = 0;
  uint32_t address;
  uint32_t input_color_mode = 0;
  uint32_t bpp;
  uint32_t output_color_mode = 0;

  /* Get bitmap data address offset */
  index = pbmp[10] + (pbmp[11] << 8) + (pbmp[12] << 16)  + (pbmp[13] << 24);

  /* Read bitmap width */
  width = pbmp[18] + (pbmp[19] << 8) + (pbmp[20] << 16)  + (pbmp[21] << 24);

  /* Read bitmap height */
  height = pbmp[22] + (pbmp[23] << 8) + (pbmp[24] << 16)  + (pbmp[25] << 24);

  /* Read bit/pixel */
  bit_pixel = pbmp[28] + (pbmp[29] << 8);

  /* Set the address */
  output_color_mode = pixel_format_table[ltdc_layer_info[LayerIndex].format_index].pixel_format;
  bpp = pixel_format_table[ltdc_layer_info[LayerIndex].format_index].bpp;
  address = (uint32_t)ltdc_layer_info[LayerIndex].fbaddr + (((MICROPY_HW_LTDC_WIDTH*Ypos) + Xpos)*(bpp));

  /* Get the layer pixel format */
  if ((bit_pixel/8) == 4)
  {
    input_color_mode = DMA2D_INPUT_ARGB8888;
  }
  else if ((bit_pixel/8) == 2)
  {
    input_color_mode = DMA2D_INPUT_RGB565;
  }
  else
  {
    input_color_mode = DMA2D_INPUT_RGB888;
  }

  /* Bypass the bitmap header */
  pbmp += (index + (width * (height - 1) * (bit_pixel/8)));

  /* Convert picture to ARGB8888 pixel format */
  for(index=0; index < height; index++)
  {
    /* Pixel format conversion */
	  LL_ConvertLineFormat((uint32_t *)pbmp, (uint32_t *)address, width, input_color_mode, output_color_mode);

    /* Increment the source and destination buffers */
    address+=  (MICROPY_HW_LTDC_WIDTH*bpp);
    pbmp -= width*(bit_pixel/8);
  }
}



/********************************************************************************
 *
 * Micro python drawprop method.
 *
 ********************************************************************************/

STATIC mp_obj_t py_ltdc_drawprop_set_fgcolor(mp_obj_t self_in, mp_obj_t color)
{
	int i;
	ltdc_drawprop_t *self = MP_OBJ_TO_PTR(self_in);

	if(MP_OBJ_IS_STR(color))
	{
		const char *color_str = mp_obj_str_get_str(color);

		for(i = 0; i < STD_COLOR_NUM; i++)
		{
			if(strcmp(color_str, std_color_table[i].color_str) == 0)
			{
				self->fgcolor = std_color_table[i].argb8888;
				break;
			}
		}

		if(i == STD_COLOR_NUM)
		{
			return mp_const_false;
		}
	}
	else if(MP_OBJ_IS_INT(color))
	{
		uint32_t color_uint = mp_obj_int_get_truncated(color);
		self->fgcolor = color_uint;
	}

    return mp_const_true;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_2(py_ltdc_drawprop_set_fgcolor_obj, py_ltdc_drawprop_set_fgcolor);


STATIC mp_obj_t py_ltdc_drawprop_set_bgcolor(mp_obj_t self_in, mp_obj_t color)
{
	int i;
	ltdc_drawprop_t *self = MP_OBJ_TO_PTR(self_in);

	if(MP_OBJ_IS_STR(color))
	{
		const char *color_str = mp_obj_str_get_str(color);

		for(i = 0; i < STD_COLOR_NUM; i++)
		{
			if(strcmp(color_str, std_color_table[i].color_str) == 0)
			{
				self->bgcolor = std_color_table[i].argb8888;
				break;
			}
		}

		if(i == STD_COLOR_NUM)
		{
			return mp_const_false;
		}
	}
	else if(MP_OBJ_IS_INT(color))
	{
		uint32_t color_uint = mp_obj_int_get_truncated(color);
		self->bgcolor = color_uint;
	}

    return mp_const_true;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_2(py_ltdc_drawprop_set_bgcolor_obj, py_ltdc_drawprop_set_bgcolor);


STATIC mp_obj_t py_ltdc_drawprop_set_font(mp_obj_t self_in, mp_obj_t fontname_obj)
{
	int i;
	ltdc_drawprop_t *self = MP_OBJ_TO_PTR(self_in);

	if(MP_OBJ_IS_STR(fontname_obj))
	{
		const char *fontname_str = mp_obj_str_get_str(fontname_obj);

		for(i = 0; i < STD_FONT_NUM; i++)
		{
			if(strcmp(fontname_str, std_font_table[i].fontname) == 0)
			{
				self->font = &std_font_table[i];
				break;
			}
		}

		if(i == STD_FONT_NUM)
		{
			return mp_const_notimplemented;
		}
	}
	else if(MP_OBJ_IS_INT(fontname_obj))
	{
		uint32_t font_index = mp_obj_int_get_truncated(fontname_obj);

		if(font_index >= STD_FONT_NUM)
		{
			return mp_const_notimplemented;
		}

		self->font = &std_font_table[font_index];
	}

    return mp_const_true;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_2(py_ltdc_drawprop_set_font_obj, py_ltdc_drawprop_set_font);


STATIC mp_obj_t py_ltdc_drawprop_clear(mp_obj_t self_in)
{
	ltdc_drawprop_t *self = MP_OBJ_TO_PTR(self_in);

	LL_FillBuffer(self->layer_index, 0, 0, MICROPY_HW_LTDC_WIDTH, MICROPY_HW_LTDC_HEIGHT, 0, self->bgcolor);

    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(py_ltdc_drawprop_clear_obj, py_ltdc_drawprop_clear);

STATIC mp_obj_t py_ltdc_drawprop_pixel(size_t n_args, const mp_obj_t *args)
{
	ltdc_drawprop_t *self = MP_OBJ_TO_PTR(args[0]);
	uint32_t x, y, color;

	x = mp_obj_get_int(args[1]);
	y = mp_obj_get_int(args[2]);

	if(n_args > 3)
	{
		color = mp_obj_int_get_truncated(args[3]);
		self->fgcolor = color;
	}
	else
	{
		color = self->fgcolor;
	}

	ltdc_draw_pixel(self->layer_index, x, y, color);

    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(py_ltdc_drawprop_pixel_obj, 3, 4, py_ltdc_drawprop_pixel);


STATIC mp_obj_t py_ltdc_drawprop_hline(size_t n_args, const mp_obj_t *args)
{
	ltdc_drawprop_t *self = MP_OBJ_TO_PTR(args[0]);
	uint32_t x, y, len, color;

	x = mp_obj_get_int(args[1]);
	y = mp_obj_get_int(args[2]);
	len = mp_obj_get_int(args[3]);

	if(n_args > 4)
	{
		color = mp_obj_int_get_truncated(args[4]);
		self->fgcolor = color;
	}
	else
	{
		color = self->fgcolor;
	}

	ltdc_draw_hline(self->layer_index, x, y, len, color);

    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(py_ltdc_drawprop_hline_obj, 4, 5, py_ltdc_drawprop_hline);

STATIC mp_obj_t py_ltdc_drawprop_vline(size_t n_args, const mp_obj_t *args)
{
	ltdc_drawprop_t *self = MP_OBJ_TO_PTR(args[0]);
	uint32_t x, y, len, color;

	x = mp_obj_get_int(args[1]);
	y = mp_obj_get_int(args[2]);
	len = mp_obj_get_int(args[3]);

	if(n_args > 4)
	{
		color = mp_obj_int_get_truncated(args[4]);
		self->fgcolor = color;
	}
	else
	{
		color = self->fgcolor;
	}

	ltdc_draw_vline(self->layer_index, x, y, len, color);

    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(py_ltdc_drawprop_vline_obj, 4, 5, py_ltdc_drawprop_vline);

STATIC mp_obj_t py_ltdc_drawprop_line(size_t n_args, const mp_obj_t *args)
{
	ltdc_drawprop_t *self = MP_OBJ_TO_PTR(args[0]);
	uint32_t x1, y1, x2, y2, color;

	x1 = mp_obj_get_int(args[1]);
	y1 = mp_obj_get_int(args[2]);
	x2 = mp_obj_get_int(args[3]);
	y2 = mp_obj_get_int(args[4]);

	if(n_args > 5)
	{
		color = mp_obj_int_get_truncated(args[5]);
		self->fgcolor = color;
	}
	else
	{
		color = self->fgcolor;
	}

	ltdc_draw_line(self->layer_index, x1, y1, x2, y2, color);

    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(py_ltdc_drawprop_line_obj, 5, 6, py_ltdc_drawprop_line);

STATIC mp_obj_t py_ltdc_drawprop_rect(size_t n_args, const mp_obj_t *args)
{
	ltdc_drawprop_t *self = MP_OBJ_TO_PTR(args[0]);
	uint32_t x, y, w, h, color;

	x = mp_obj_get_int(args[1]);
	y = mp_obj_get_int(args[2]);
	w = mp_obj_get_int(args[3]);
	h = mp_obj_get_int(args[4]);

	if(n_args > 5)
	{
		color = mp_obj_int_get_truncated(args[5]);
		self->fgcolor = color;
	}
	else
	{
		color = self->fgcolor;
	}

	ltdc_draw_rect(self->layer_index, x, y, w, h, color);

    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(py_ltdc_drawprop_rect_obj, 5, 6, py_ltdc_drawprop_rect);



STATIC mp_obj_t py_ltdc_drawprop_circle(size_t n_args, const mp_obj_t *args)
{
	ltdc_drawprop_t *self = MP_OBJ_TO_PTR(args[0]);
	uint32_t x, y, r, color;

	x = mp_obj_get_int(args[1]);
	y = mp_obj_get_int(args[2]);
	r = mp_obj_get_int(args[3]);

	if(n_args > 4)
	{
		color = mp_obj_int_get_truncated(args[4]);
		self->fgcolor = color;
	}
	else
	{
		color = self->fgcolor;
	}

	ltdc_draw_circle(self->layer_index, x, y, r, color);

    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(py_ltdc_drawprop_circle_obj, 4, 5, py_ltdc_drawprop_circle);


STATIC mp_obj_t py_ltdc_drawprop_ellipse(size_t n_args, const mp_obj_t *args)
{
	ltdc_drawprop_t *self = MP_OBJ_TO_PTR(args[0]);
	int x, y, rx, ry;
	uint32_t color;


	x = mp_obj_get_int(args[1]);
	y = mp_obj_get_int(args[2]);
	rx = mp_obj_get_int(args[3]);
	ry = mp_obj_get_int(args[4]);

	if(n_args > 5)
	{
		color = mp_obj_int_get_truncated(args[5]);
		self->fgcolor = color;
	}
	else
	{
		color = self->fgcolor;
	}

	ltdc_draw_ellipse(self->layer_index, x, y, rx, ry, color);

    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(py_ltdc_drawprop_ellipse_obj, 5, 6, py_ltdc_drawprop_ellipse);


/**
  * @brief  Draws an poly-line (between many points).
  * @param  Points: Pointer to the points array
  * @param  PointCount: Number of points
  * @retval None
  */
STATIC mp_obj_t py_ltdc_drawprop_polygon(size_t n_args, const mp_obj_t *args)
{
	ltdc_drawprop_t *self = MP_OBJ_TO_PTR(args[0]);
	int x1 = 0, y1 = 0, x2 = 0, y2 = 0;
	uint32_t color;

	if(!MP_OBJ_IS_TYPE(args[1], &mp_type_list))
	{
		return mp_const_none;
	}

	if(n_args > 2)
	{
		color = mp_obj_int_get_truncated(args[2]);
		self->fgcolor = color;
	}
	else
	{
		color = self->fgcolor;
	}

	mp_obj_list_t *list = MP_OBJ_TO_PTR(args[1]);
	mp_obj_list_t *sublist;

	/* Get Xn Yn */
	sublist = MP_OBJ_TO_PTR(list->items[list->len - 1]);
	x2 = mp_obj_get_int(sublist->items[0]);
	y2 = mp_obj_get_int(sublist->items[1]);

	/* Get X1 Y1 */
	sublist = MP_OBJ_TO_PTR(list->items[0]);
	x1 = mp_obj_get_int(sublist->items[0]);
	y1 = mp_obj_get_int(sublist->items[1]);

	ltdc_draw_line(self->layer_index, x1, y1, x2, y2, color);

	for(int i = 0; i < list->len - 1; i++)
	{
		sublist = MP_OBJ_TO_PTR(list->items[i]);
		x1 = mp_obj_get_int(sublist->items[0]);
		y1 = mp_obj_get_int(sublist->items[1]);

		sublist = MP_OBJ_TO_PTR(list->items[i + 1]);
		x2 = mp_obj_get_int(sublist->items[0]);
		y2 = mp_obj_get_int(sublist->items[1]);
		ltdc_draw_line(self->layer_index, x1, y1, x2, y2, color);
	}

	return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(py_ltdc_drawprop_polygon_obj, 2, 3, py_ltdc_drawprop_polygon);

STATIC mp_obj_t py_ltdc_drawprop_fillrect(size_t n_args, const mp_obj_t *args)
{
	ltdc_drawprop_t *self = MP_OBJ_TO_PTR(args[0]);
	uint32_t x, y, xsize, ysize, color;

	x = mp_obj_get_int(args[1]);
	y = mp_obj_get_int(args[2]);
	xsize = mp_obj_get_int(args[3]);
	ysize = mp_obj_get_int(args[4]);

	if(n_args > 5)
	{
		color = mp_obj_int_get_truncated(args[5]);
		self->fgcolor = color;
	}
	else
	{
		color = self->fgcolor;
	}

	ltdc_draw_fillrect(self->layer_index, x, y, xsize, ysize, color);

    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(py_ltdc_drawprop_fillrect_obj, 5, 6, py_ltdc_drawprop_fillrect);

STATIC mp_obj_t py_ltdc_drawprop_fillcircle(size_t n_args, const mp_obj_t *args)
{
	ltdc_drawprop_t *self = MP_OBJ_TO_PTR(args[0]);
	uint32_t x, y, r, color;

	x = mp_obj_get_int(args[1]);
	y = mp_obj_get_int(args[2]);
	r = mp_obj_get_int(args[3]);

	if(n_args > 4)
	{
		color = mp_obj_int_get_truncated(args[4]);
		self->fgcolor = color;
	}
	else
	{
		color = self->fgcolor;
	}

	ltdc_draw_fillcircle(self->layer_index, x, y, r, color);

    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(py_ltdc_drawprop_fillcircle_obj, 4, 5, py_ltdc_drawprop_fillcircle);

STATIC mp_obj_t py_ltdc_drawprop_fillellipse(size_t n_args, const mp_obj_t *args)
{
	ltdc_drawprop_t *self = MP_OBJ_TO_PTR(args[0]);
	uint32_t x, y, rx, ry, color;

	x = mp_obj_get_int(args[1]);
	y = mp_obj_get_int(args[2]);
	rx = mp_obj_get_int(args[3]);
	ry = mp_obj_get_int(args[4]);

	if(n_args > 5)
	{
		color = mp_obj_int_get_truncated(args[5]);
		self->fgcolor = color;
	}
	else
	{
		color = self->fgcolor;
	}

	ltdc_draw_fillellipse(self->layer_index, x, y, rx, ry, color);

    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(py_ltdc_drawprop_fillellipse_obj, 5, 6, py_ltdc_drawprop_fillellipse);


/**
  * @brief  Draws an poly-line (between many points).
  * @param  Points: Pointer to the points array
  * @param  PointCount: Number of points
  * @retval None
  */
STATIC mp_obj_t py_ltdc_drawprop_fillpolygon(size_t n_args, const mp_obj_t *args)
{
	ltdc_drawprop_t *self = MP_OBJ_TO_PTR(args[0]);
	int X = 0, Y = 0, X2 = 0, Y2 = 0, X_center = 0, Y_center = 0, X_first = 0, Y_first = 0, pixelX = 0, pixelY = 0, counter = 0;
	uint32_t  image_left = 0, image_right = 0, image_top = 0, image_bottom = 0, color;

	if(!MP_OBJ_IS_TYPE(args[1], &mp_type_list))
	{
		return mp_const_none;
	}

	if(n_args > 2)
	{
		color = mp_obj_int_get_truncated(args[2]);
		self->fgcolor = color;
	}
	else
	{
		color = self->fgcolor;
	}

	mp_obj_list_t *list = MP_OBJ_TO_PTR(args[1]);
	mp_obj_list_t *sublist;

	sublist = MP_OBJ_TO_PTR(list->items[0]);
	image_left = image_right = mp_obj_get_int(sublist->items[0]);
	image_top = image_bottom = mp_obj_get_int(sublist->items[1]);

	X_first = mp_obj_get_int(sublist->items[0]);
	Y_first = mp_obj_get_int(sublist->items[1]);

	for(counter = 1; counter < list->len; counter++)
	{
		sublist = MP_OBJ_TO_PTR(list->items[counter]);
	    pixelX = mp_obj_get_int(sublist->items[0]);
	    if(pixelX < image_left)
	    {
	    	image_left = pixelX;
	    }
	    if(pixelX > image_right)
	    {
	    	image_right = pixelX;
	    }

	    pixelY = mp_obj_get_int(sublist->items[1]);
	    if(pixelY < image_top)
	    {
	    	image_top = pixelY;
	    }
	    if(pixelY > image_bottom)
	    {
	    	image_bottom = pixelY;
	    }
	}

	if(list->len < 2)
	{
		return mp_const_none;
	}

	X_center = (image_left + image_right)/2;
	Y_center = (image_bottom + image_top)/2;

	for(int i = 0; i < list->len - 1; i++)
	{
		sublist = MP_OBJ_TO_PTR(list->items[i]);
		X = mp_obj_get_int(sublist->items[0]);
		Y = mp_obj_get_int(sublist->items[1]);

		sublist = MP_OBJ_TO_PTR(list->items[i + 1]);
		X2 = mp_obj_get_int(sublist->items[0]);
		Y2 = mp_obj_get_int(sublist->items[1]);

		FillTriangle(self->layer_index, X, X2, X_center, Y, Y2, Y_center, color);
		FillTriangle(self->layer_index, X, X_center, X2, Y, Y_center, Y2, color);
		FillTriangle(self->layer_index, X_center, X2, X, Y_center, Y2, Y, color);
	}

	FillTriangle(self->layer_index, X_first, X2, X_center, Y_first, Y2, Y_center, color);
	FillTriangle(self->layer_index, X_first, X_center, X2, Y_first, Y_center, Y2, color);
	FillTriangle(self->layer_index, X_center, X2, X_first, Y_center, Y2, Y_first, color);

	return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(py_ltdc_drawprop_fillpolygon_obj, 2, 3, py_ltdc_drawprop_fillpolygon);


STATIC mp_obj_t py_ltdc_drawprop_string(size_t n_args, const mp_obj_t *args)
{
	ltdc_drawprop_t *self = MP_OBJ_TO_PTR(args[0]);
	uint32_t x, y, color;
	align_mode_t align;
	const char *s;

	x = mp_obj_get_int(args[1]);
	y = mp_obj_get_int(args[2]);

	if(!MP_OBJ_IS_STR(args[3]))
		return mp_const_none;

	s = mp_obj_str_get_str(args[3]);

	if(n_args > 4)
	{
		color = mp_obj_int_get_truncated(args[4]);
		self->fgcolor = color;
	}
	else
	{
		color = self->fgcolor;
	}

	if(n_args > 5)
	{
		switch(mp_obj_get_int(args[5]))
		{
		case 1:
			align = CENTER_MODE;
			break;
		case 2:
			align = RIGHT_MODE;
			break;
		case 3:
		default:
			align = LEFT_MODE;
			break;
		}
	}
	else
	{
		align = LEFT_MODE;
	}

	ltdc_draw_string(self->layer_index, self->font, 0, x, y, s, align, color, self->bgcolor);

    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(py_ltdc_drawprop_string_obj, 4, 6, py_ltdc_drawprop_string);


STATIC mp_obj_t py_ltdc_drawprop_stringline(size_t n_args, const mp_obj_t *args)
{
	ltdc_drawprop_t *self = MP_OBJ_TO_PTR(args[0]);
	uint32_t line, color;
	const char *s;

	line = mp_obj_get_int(args[1]);

	if(!MP_OBJ_IS_STR(args[2]))
		return mp_const_none;

	s = mp_obj_str_get_str(args[2]);

	if(n_args > 3)
	{
		color = mp_obj_int_get_truncated(args[3]);
		self->fgcolor = color;
	}
	else
	{
		color = self->fgcolor;
	}

	ltdc_draw_string(self->layer_index, self->font, 0, 0, line*self->font->font->Height, s, LEFT_MODE, color, self->bgcolor);

    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(py_ltdc_drawprop_stringline_obj, 3, 4, py_ltdc_drawprop_stringline);

STATIC mp_obj_t py_ltdc_drawprop_clear_stringline(mp_obj_t self_in, mp_obj_t line)
{
	int line_int;
	ltdc_drawprop_t *self = MP_OBJ_TO_PTR(self_in);

	line_int = mp_obj_get_int(line);

	LL_FillBuffer(self->layer_index, 0, line_int*self->font->font->Height, MICROPY_HW_LTDC_WIDTH, self->font->font->Height, 0, self->bgcolor);

    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_2(py_ltdc_drawprop_clear_stringline_obj, py_ltdc_drawprop_clear_stringline);


#define FILE_BUFFER_SIZE	256
STATIC mp_obj_t file_readall(mp_obj_t fileio)
{
    const mp_stream_p_t *stream_p = mp_get_stream(fileio);

    mp_uint_t total_size = 0;
    vstr_t vstr;
    vstr_init(&vstr, FILE_BUFFER_SIZE);
    char *p = vstr.buf;
    mp_uint_t current_read = FILE_BUFFER_SIZE;
    while (true) {
        int error;
        mp_uint_t out_sz = stream_p->read(fileio, p, current_read, &error);
        if (out_sz == MP_STREAM_ERROR) {
            if (mp_is_nonblocking_error(error)) {
                // With non-blocking streams, we read as much as we can.
                // If we read nothing, return None, just like read().
                // Otherwise, return data read so far.
                if (total_size == 0) {
                    return mp_const_none;
                }
                break;
            }
            mp_raise_OSError(error);
        }
        if (out_sz == 0) {
            break;
        }
        total_size += out_sz;
        if (out_sz < current_read) {
            current_read -= out_sz;
            p += out_sz;
        } else {
            p = vstr_extend(&vstr, FILE_BUFFER_SIZE);
            current_read = FILE_BUFFER_SIZE;
        }
    }

    vstr.len = total_size;
    return mp_obj_new_str_from_vstr(&mp_type_bytes, &vstr);
}

STATIC mp_obj_t py_ltdc_drawprop_bitmap(size_t n_args, const mp_obj_t *args)
{
	mp_obj_t file = mp_const_none;
	mp_obj_t data = mp_const_none;
	const char *filename_str;
	int need_openfile = 0, need_readfile = 0;
	ltdc_drawprop_t *self = MP_OBJ_TO_PTR(args[0]);
	uint32_t x, y;

	x = mp_obj_get_int(args[1]);
	y = mp_obj_get_int(args[2]);

	if(MP_OBJ_IS_STR(args[3]))
	{
		filename_str = mp_obj_str_get_str(args[3]);
		need_openfile = 1;
		need_readfile = 1;
	}
	else if(MP_OBJ_IS_TYPE(args[3], &mp_type_vfs_fat_fileio) || MP_OBJ_IS_TYPE(args[3], &mp_type_vfs_fat_fileio))
	{
		file = args[3];
		need_readfile = 1;
	}
	else if(MP_OBJ_IS_TYPE(args[3], &mp_type_bytes))
	{
		data = args[3];
	}


	if(need_openfile)
	{
		mp_obj_t open_args[2];
		mp_obj_t filename_obj;

		filename_obj = mp_obj_new_str(filename_str, strlen(filename_str));

		if(MP_OBJ_IS_STR(filename_obj))
		{
			open_args[0] = filename_obj;
			open_args[1] = MP_ROM_QSTR(MP_QSTR_rb);

			file = mp_vfs_open(2, open_args, (mp_map_t*)&mp_const_empty_map);
			m_del_obj(&mp_type_str, filename_obj);
		}
	}

	if(need_readfile)
	{
		if(MP_OBJ_IS_TYPE(file, &mp_type_vfs_fat_fileio))
		{
			data = file_readall(file);

			if(need_openfile)
			{
				mp_stream_close(file);
			}
			else
			{
				int error;
				const mp_stream_p_t *stream_p = mp_get_stream(file);
				struct mp_stream_seek_t seek;

				seek.whence = MP_SEEK_SET;
				seek.offset = 0;
				stream_p->ioctl(file, MP_STREAM_SEEK, (uintptr_t)&seek, &error);
			}

		}
	}

	if(MP_OBJ_IS_TYPE(data, &mp_type_bytes))
	{
		size_t len;
		const char *byte = mp_obj_str_get_data(data, &len);
		ltdc_draw_bitmap(self->layer_index, x, y, (unsigned char*)byte);

		if(need_readfile)
		{
			m_del_obj(&mp_type_bytes, data);
		}
	}

    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(py_ltdc_drawprop_bitmap_obj, 4, 4, py_ltdc_drawprop_bitmap);

// creating the table of global members
STATIC const mp_rom_map_elem_t ltdc_layer_locals_dict_table[] = {
		{MP_ROM_QSTR(MP_QSTR_set_fgcolor), MP_ROM_PTR(&py_ltdc_drawprop_set_fgcolor_obj)},
		{MP_ROM_QSTR(MP_QSTR_set_bgcolor), MP_ROM_PTR(&py_ltdc_drawprop_set_bgcolor_obj)},
		{MP_ROM_QSTR(MP_QSTR_set_font), MP_ROM_PTR(&py_ltdc_drawprop_set_font_obj)},
		{MP_ROM_QSTR(MP_QSTR_clear), MP_ROM_PTR(&py_ltdc_drawprop_clear_obj)},
		{MP_ROM_QSTR(MP_QSTR_pixel), MP_ROM_PTR(&py_ltdc_drawprop_pixel_obj)},
		{MP_ROM_QSTR(MP_QSTR_hline), MP_ROM_PTR(&py_ltdc_drawprop_hline_obj)},
		{MP_ROM_QSTR(MP_QSTR_vline), MP_ROM_PTR(&py_ltdc_drawprop_vline_obj)},
		{MP_ROM_QSTR(MP_QSTR_line), MP_ROM_PTR(&py_ltdc_drawprop_line_obj)},
		{MP_ROM_QSTR(MP_QSTR_rect), MP_ROM_PTR(&py_ltdc_drawprop_rect_obj)},
		{MP_ROM_QSTR(MP_QSTR_circle), MP_ROM_PTR(&py_ltdc_drawprop_circle_obj)},
		{MP_ROM_QSTR(MP_QSTR_ellipse), MP_ROM_PTR(&py_ltdc_drawprop_ellipse_obj)},
		{MP_ROM_QSTR(MP_QSTR_polygon), MP_ROM_PTR(&py_ltdc_drawprop_polygon_obj)},
		{MP_ROM_QSTR(MP_QSTR_fillrect), MP_ROM_PTR(&py_ltdc_drawprop_fillrect_obj)},
		{MP_ROM_QSTR(MP_QSTR_fillcircle), MP_ROM_PTR(&py_ltdc_drawprop_fillcircle_obj)},
		{MP_ROM_QSTR(MP_QSTR_fillellipse), MP_ROM_PTR(&py_ltdc_drawprop_fillellipse_obj)},
		{MP_ROM_QSTR(MP_QSTR_fillpolygon), MP_ROM_PTR(&py_ltdc_drawprop_fillpolygon_obj)},
		{MP_ROM_QSTR(MP_QSTR_string), MP_ROM_PTR(&py_ltdc_drawprop_string_obj)},
		{MP_ROM_QSTR(MP_QSTR_stringline), MP_ROM_PTR(&py_ltdc_drawprop_stringline_obj)},
		{MP_ROM_QSTR(MP_QSTR_clearline), MP_ROM_PTR(&py_ltdc_drawprop_clear_stringline_obj)},
		{MP_ROM_QSTR(MP_QSTR_bitmap), MP_ROM_PTR(&py_ltdc_drawprop_bitmap_obj)},
};
STATIC MP_DEFINE_CONST_DICT(ltdc_layer_locals_dict, ltdc_layer_locals_dict_table);

STATIC mp_obj_t py_ltdc_create_drawprop(const mp_obj_type_t *type,
        								size_t n_args,
										size_t n_kw,
										const mp_obj_t *args);
STATIC void py_ltdc_drawprop_print(const mp_print_t *print, mp_obj_t self_in,
                                mp_print_kind_t kind);

// create the class-object itself
const mp_obj_type_t ltdc_drawprop_obj_type = {
    // "inherit" the type "type"
    { &mp_type_type },
     // give it a name
    .name = MP_QSTR_drawprop,
     // give it a print-function
    .print = py_ltdc_drawprop_print,
     // give it a constructor
    .make_new = py_ltdc_create_drawprop,
     // and the global members
    .locals_dict = (mp_obj_dict_t*)&ltdc_layer_locals_dict,
};


STATIC mp_obj_t py_ltdc_create_drawprop(const mp_obj_type_t *type,
        								size_t n_args,
										size_t n_kw,
										const mp_obj_t *args)
{
	int layer_index;

    // this checks the number of arguments (min 1, max 1);
    // on error -> raise python exception
	mp_arg_check_num(n_args, n_kw, 1, 4, true);

	layer_index = mp_obj_get_int(args[0]);

	if(layer_index < LTDC_MAX_LAYER_NUM)
	{
		// create a new object of our C-struct type
		ltdc_drawprop_t *self = m_new_obj(ltdc_drawprop_t);

		// give it a type
		self->base.type = &ltdc_drawprop_obj_type;

		self->layer_index = layer_index;

		self->fgcolor = 0xFFFFFFFF;
		self->bgcolor = 0xFF000000;
		self->font = LCD_DEFAULT_FONT;

		if(n_args > 1)
		{
			self->fgcolor = mp_obj_int_get_truncated(args[1]);
		}

		if(n_args > 2)
		{
			self->bgcolor = mp_obj_int_get_truncated(args[2]);
		}

		if(n_args > 3)
		{
			py_ltdc_drawprop_set_font(self, args[3]);
		}

		return MP_OBJ_FROM_PTR(self);
	}
	else
	{
		return MP_OBJ_NULL;
	}
}

STATIC void py_ltdc_drawprop_print(const mp_print_t *print, mp_obj_t self_in,
                                mp_print_kind_t kind)
{
	// get a ptr to the C-struct of the object
	ltdc_drawprop_t *self = MP_OBJ_TO_PTR(self_in);
	const char *format_string;
	const char *visible_str;

	format_string = pixel_format_table[ltdc_layer_info[self->layer_index].format_index].format_string;
	if(ltdc_layer_info[self->layer_index].visible)
		visible_str = "Visible";
	else
		visible_str = "Invisible";

	if(ltdc_layer_info[self->layer_index].init)
		printf ("Active Layer : %u (%s,%s,%u)\r\n", self->layer_index, format_string, visible_str, ltdc_layer_info[self->layer_index].alpha);
	else
		printf ("Active Layer : %u (Not ready)\r\n", self->layer_index);

	printf ("Foreground Color : 0x%08X\r\n", (unsigned int)self->fgcolor);
	printf ("Background Color : 0x%08X\r\n", (unsigned int)self->bgcolor);
	printf ("Font : %s\r\n", self->font->fontname);
}


/********************************************************************************
 *
 * Micro python wrap function.
 *
 ********************************************************************************/

STATIC mp_obj_t py_ltdc_init(void)
{
	ltdc_init();
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_0(py_ltdc_init_obj, py_ltdc_init);

STATIC mp_obj_t py_ltdc_backlight(mp_obj_t state)
{
	int bl_val;

	if(MP_OBJ_IS_INT(state))
	{
		bl_val = mp_obj_get_int(state);

		if(bl_val >= 0 && bl_val <= 100)
		{
			lcd_backlight(bl_val);
		}
	}

    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(py_ltdc_backlight_obj, py_ltdc_backlight);

STATIC mp_obj_t py_ltdc_enable(void)
{
	ltdc_enable();
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_0(py_ltdc_enable_obj, py_ltdc_enable);

STATIC mp_obj_t py_ltdc_disable(void)
{
	ltdc_disable();
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_0(py_ltdc_disable_obj, py_ltdc_disable);

STATIC mp_obj_t py_ltdc_fontlist(void)
{
	int i;

	printf("Fonts list:\r\n");

	for(i = 0; i < STD_FONT_NUM; i++)
	{
		printf("%d - %s\r\n", i, std_font_table[i].fontname);
	}

    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_0(py_ltdc_fontlist_obj, py_ltdc_fontlist);

STATIC mp_obj_t py_ltdc_layer_config(size_t n_args, const mp_obj_t *pos_args, mp_map_t *kw_args)
{
	enum { ARG_index=0, ARG_format=1 };
	static const mp_arg_t allowed_args[] = {
			{MP_QSTR_index, MP_ARG_REQUIRED | MP_ARG_OBJ},
			{MP_QSTR_format, MP_ARG_OBJ, {.u_obj = MP_ROM_INT(0)}},
	};

	mp_arg_val_t args[MP_ARRAY_SIZE(allowed_args)];
	mp_arg_parse_all(	n_args,     /* The number of argument to be parsed. */
						pos_args,   /* Pointer to the argument. */
						kw_args,
						MP_ARRAY_SIZE(allowed_args),
						allowed_args,
						args);

	int format_index = 0;
	int layer_index = 0;
	int i;

	if(MP_OBJ_IS_STR(args[ARG_index].u_obj))
	{
    	const char *layer_index_str = mp_obj_str_get_str(args[ARG_index].u_obj);
    	int layer_index_strlen = strlen(layer_index_str);

		for(i = 0; i < layer_index_strlen; i++)
		{
			layer_index = layer_index*10 + layer_index_str[i];
		}

		if(layer_index < 0 || layer_index >= LTDC_MAX_LAYER_NUM)
			return mp_const_false;
	}
	else if(MP_OBJ_IS_INT(args[ARG_index].u_obj))
	{
		layer_index = mp_obj_get_int(args[ARG_index].u_obj);
		if(layer_index < 0 || layer_index >= LTDC_MAX_LAYER_NUM)
			return mp_const_false;
	}
	else
	{
		return mp_const_false;
	}

	if(args[ARG_format].u_obj != MP_OBJ_NULL)
	{
		if(MP_OBJ_IS_STR(args[ARG_format].u_obj))
		{
			const char *format_str = mp_obj_str_get_str(args[ARG_format].u_obj);
			for(i = 0; i < PIXEL_FORMAT_NUM; i++)
			{
				if (strcmp(format_str, pixel_format_table[i].format_string) == 0)
        		{
					format_index = i;
					break;
        		}
			}

			if(i == PIXEL_FORMAT_NUM)
			{
        		return mp_const_false;
			}
		}
		else if(MP_OBJ_IS_INT(args[ARG_format].u_obj))
		{
			format_index = mp_obj_get_int(args[ARG_format].u_obj);
			if(format_index < 0 || format_index >= PIXEL_FORMAT_NUM)
				return mp_const_false;
		}
		else
		{
			return mp_const_false;
		}
	}

	if(ltdc_layer_info[layer_index].fbaddr && ltdc_layer_info[layer_index].fbsize < pixel_format_table[format_index].require_fbsize)
	{
		return mp_const_false;
	}

	if(ltdc_layer_info[layer_index].init == 0 || ltdc_layer_info[layer_index].format_index != format_index)
	{
		ltdc_layer_init(layer_index, format_index);

		ltdc_layer_info[layer_index].init = 1;
		ltdc_layer_info[layer_index].format_index = format_index;
	}

	return mp_const_true;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_KW(py_ltdc_layer_config_obj, 1, py_ltdc_layer_config);



STATIC mp_obj_t py_ltdc_layer_visible(mp_obj_t layer, mp_obj_t visible)
{
	int layer_index;
	FunctionalState State;

	if(MP_OBJ_IS_INT(layer))
	{
		layer_index = mp_obj_get_int(layer);

		if(layer_index >= 0 && layer_index < LTDC_MAX_LAYER_NUM)
		{
			/* Get old visible state */
			if(ltdc_layer_info[layer_index].visible)
				State = ENABLE;
			else
				State = DISABLE;

			if(MP_OBJ_IS_INT(visible))
			{
				if(mp_obj_get_int(visible))
					State = ENABLE;
				else
					State = DISABLE;
			}
			else if(MP_OBJ_IS_TYPE(visible, &mp_type_bool))
			{
				if((mp_obj_t)&mp_const_true_obj == visible)
					State = ENABLE;
				else
					State = DISABLE;
			}

			ltdc_layer_visible(layer_index, State);

			/* Keep new visible state */
			if(State == ENABLE)
				ltdc_layer_info[layer_index].visible = 1;
			else
				ltdc_layer_info[layer_index].visible = 0;
		}
	}

    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_2(py_ltdc_layer_visible_obj, py_ltdc_layer_visible);

STATIC mp_obj_t py_ltdc_layer_alpha(mp_obj_t layer, mp_obj_t alpha)
{
	unsigned int alpha_int;
	int layer_index;

	if(MP_OBJ_IS_INT(layer))
	{
		layer_index = mp_obj_get_int(layer);

		if(layer_index >= 0 && layer_index < LTDC_MAX_LAYER_NUM)
		{
			/* Get old alpha state. */
			alpha_int = ltdc_layer_info[layer_index].alpha;

			if(MP_OBJ_IS_INT(alpha))
				alpha_int = mp_obj_get_int(alpha);

			if(alpha_int > 255)
				alpha_int = 255;

			/* Keep new alpha state. */
			ltdc_layer_info[layer_index].alpha = (alpha_int & 0xFF);
			ltdc_layer_alpha(layer_index, ltdc_layer_info[layer_index].alpha);
		}
	}

    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_2(py_ltdc_layer_alpha_obj, py_ltdc_layer_alpha);

/*************************************************************************/

STATIC const mp_map_elem_t ltdc_globals_table[] = {
    { MP_OBJ_NEW_QSTR(MP_QSTR___name__), MP_OBJ_NEW_QSTR(MP_QSTR_ltdc) },
	{ MP_OBJ_NEW_QSTR(MP_QSTR_init), (mp_obj_t)&py_ltdc_init_obj },
	{ MP_OBJ_NEW_QSTR(MP_QSTR_enable), (mp_obj_t)&py_ltdc_enable_obj },
	{ MP_OBJ_NEW_QSTR(MP_QSTR_disable), (mp_obj_t)&py_ltdc_disable_obj },
	{ MP_OBJ_NEW_QSTR(MP_QSTR_backlight), (mp_obj_t)&py_ltdc_backlight_obj },
	{ MP_OBJ_NEW_QSTR(MP_QSTR_fontlist), (mp_obj_t)&py_ltdc_fontlist_obj },

	/* Layer function. */
	{ MP_OBJ_NEW_QSTR(MP_QSTR_layer_config), (mp_obj_t)&py_ltdc_layer_config_obj },
	{ MP_OBJ_NEW_QSTR(MP_QSTR_layer_visible), (mp_obj_t)&py_ltdc_layer_visible_obj },
	{ MP_OBJ_NEW_QSTR(MP_QSTR_layer_alpha), (mp_obj_t)&py_ltdc_layer_alpha_obj },

	/* LTDC Layer class */
	{ MP_OBJ_NEW_QSTR(MP_QSTR_drawprop), (mp_obj_t)&ltdc_drawprop_obj_type },
};

STATIC MP_DEFINE_CONST_DICT (mp_module_ltdc_globals, ltdc_globals_table);

const mp_obj_module_t mp_module_ltdc =
{
    .base = { &mp_type_module },
    .globals = (mp_obj_dict_t*)&mp_module_ltdc_globals,
};



#endif /* MICROPY_HW_ENABLE_LTDC */
