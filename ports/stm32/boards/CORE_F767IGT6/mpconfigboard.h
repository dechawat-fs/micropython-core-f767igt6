// This board is only confirmed to operate using DFU mode and openocd.
// DFU mode can be accessed by setting BOOT0 (see schematics)
// To use openocd run "OPENOCD_CONFIG=boards/openocd_stm32f7.cfg" in
// the make command.

#define MICROPY_HW_BOARD_NAME       "STM32F767IGT6-Core"
#define MICROPY_HW_MCU_NAME         "STM32F767IGT6@200MHz"

#define MICROPY_HW_HAS_SWITCH       (1)
#define MICROPY_HW_HAS_FLASH        (1)
#define MICROPY_HW_ENABLE_RNG       (1)
#define MICROPY_HW_ENABLE_RTC       (1)
#define MICROPY_HW_ENABLE_DAC       (1)
#define MICROPY_HW_ENABLE_USB       (1)
#define MICROPY_HW_ENABLE_LTDC      (1)

// HSE is 25MHz
// VCOClock = HSE * PLLN / PLLM = 25 MHz * 400 / 25 = 400 MHz
// SYSCLK = VCOClock / PLLP = 400 MHz / 2 = 200 MHz
// USB/SDMMC/RNG Clock = VCOClock / PLLQ = 400 MHz / 9 = 44.44444 MHz
#define MICROPY_HW_CLK_PLLM 		(25)
#define MICROPY_HW_CLK_PLLN 		(400)
#define MICROPY_HW_CLK_PLLP 		(RCC_PLLP_DIV2)
#define MICROPY_HW_CLK_PLLQ 		(9)

// HSE is 25MHz
// VCOClock = HSE * PLLSAIN / PLLM = 25 MHz * 192 / 25 = 192 MHz
// LTDC = (VCOClock / PLLSAIR) / PLLSAIDIVR = (192 MHz / 5) / 4 = 9.6 MHz
// USB/SDMMC/RNG Clock = VCOClock / PLLSAIP = 192 MHz / 4 = 48 MHz
#define MICROPY_HW_CLK_PLLSAIN 		(192)
#define MICROPY_HW_CLK_PLLSAIP 		(RCC_PLLSAIP_DIV4)
#define MICROPY_HW_CLK_PLLSAIR 		(5)
#define MICROPY_HW_CLK_PLLSAIDIVR 	(RCC_PLLSAIDIVR_4)
#define MICROPY_HW_CLK_PLLSAIQ 		(9)
#define MICROPY_HW_CLK_PLLSAIDIVQ	(1)

// From the reference manual, for 2.7V to 3.6V
// 151-180 MHz => 5 wait states
// 181-210 MHz => 6 wait states
// 211-216 MHz => 7 wait states
#define MICROPY_HW_FLASH_LATENCY    FLASH_LATENCY_6 // 210-216 MHz needs 7 wait states

// UART config
#define MICROPY_HW_UART6_TX         (pin_G14)
#define MICROPY_HW_UART6_RX         (pin_G9)

// I2C busses
#define MICROPY_HW_I2C1_SCL         (pin_B8)
#define MICROPY_HW_I2C1_SDA         (pin_B9)
#define MICROPY_HW_I2C4_SCL         (pin_D12)
#define MICROPY_HW_I2C4_SDA         (pin_D13)

// SPI
#define MICROPY_HW_SPI2_NSS         (pin_C1)
#define MICROPY_HW_SPI2_SCK         (pin_D3)
#define MICROPY_HW_SPI2_MISO        (pin_C2)
#define MICROPY_HW_SPI2_MOSI        (pin_C3)

// CAN busses
//#define MICROPY_HW_CAN1_TX          (pin_B9)
//#define MICROPY_HW_CAN1_RX          (pin_B8)
//#define MICROPY_HW_CAN2_TX          (pin_B13)
//#define MICROPY_HW_CAN2_RX          (pin_B12)

// USRSW is pulled low. Pressing the button makes the input go high.
#define MICROPY_HW_USRSW_PIN        (pin_C13)
#define MICROPY_HW_USRSW_PULL       (GPIO_NOPULL)
#define MICROPY_HW_USRSW_EXTI_MODE  (GPIO_MODE_IT_FALLING)
#define MICROPY_HW_USRSW_PRESSED    (0)

// LEDs
#define MICROPY_HW_LED1             (pin_G13) // green
#define MICROPY_HW_LED_ON(pin)      (mp_hal_pin_high(pin))
#define MICROPY_HW_LED_OFF(pin)     (mp_hal_pin_low(pin))

// SD card detect switch
#define MICROPY_HW_SDCARD_DETECT_PIN        (pin_I11)
#define MICROPY_HW_SDCARD_DETECT_PULL       (GPIO_PULLUP)
#define MICROPY_HW_SDCARD_DETECT_PRESENT    (GPIO_PIN_RESET)

// USB config (CN13 - USB OTG FS)
#define MICROPY_HW_USB_FS              (1)
//#define MICROPY_HW_USB_VBUS_DETECT_PIN (pin_A9)
#define MICROPY_HW_USB_OTG_ID_PIN      (pin_A10)

// SDRAM
// Reserve last 1020KB for LTDC
#define MICROPY_HW_SDRAM_SIZE  (((256/8) * 1024 * 1024) - (MICROPY_LTDC_LAYER0_FB_SIZE + MICROPY_LTDC_LAYER1_FB_SIZE))  // 256 Mbit
#define MICROPY_HW_SDRAM_STARTUP_TEST             (1)

// Timing configuration for 90 Mhz (11.90ns) of SD clock frequency (180Mhz/2)
#define MICROPY_HW_SDRAM_TIMING_TMRD        (2)
#define MICROPY_HW_SDRAM_TIMING_TXSR        (7)
#define MICROPY_HW_SDRAM_TIMING_TRAS        (4)
#define MICROPY_HW_SDRAM_TIMING_TRC         (7)
#define MICROPY_HW_SDRAM_TIMING_TWR         (2)
#define MICROPY_HW_SDRAM_TIMING_TRP         (2)
#define MICROPY_HW_SDRAM_TIMING_TRCD        (2)
#define MICROPY_HW_SDRAM_REFRESH_RATE       (64) // ms

#define MICROPY_HW_SDRAM_BURST_LENGTH       1
#define MICROPY_HW_SDRAM_CAS_LATENCY        3
#define MICROPY_HW_SDRAM_COLUMN_BITS_NUM    9
#define MICROPY_HW_SDRAM_ROW_BITS_NUM       13
#define MICROPY_HW_SDRAM_MEM_BUS_WIDTH      16
#define MICROPY_HW_SDRAM_INTERN_BANKS_NUM   4
#define MICROPY_HW_SDRAM_CLOCK_PERIOD       2
#define MICROPY_HW_SDRAM_RPIPE_DELAY        0
#define MICROPY_HW_SDRAM_RBURST             (1)
#define MICROPY_HW_SDRAM_WRITE_PROTECTION   (0)
#define MICROPY_HW_SDRAM_AUTOREFRESH_NUM    (8)

#define MICROPY_HW_FMC_SDCKE1   (pin_H7)
#define MICROPY_HW_FMC_SDNE1    (pin_H6)
#define MICROPY_HW_FMC_SDCLK    (pin_G8)
#define MICROPY_HW_FMC_SDNCAS   (pin_G15)
#define MICROPY_HW_FMC_SDNRAS   (pin_F11)
#define MICROPY_HW_FMC_SDNWE    (pin_C0)
#define MICROPY_HW_FMC_BA0      (pin_G4)
#define MICROPY_HW_FMC_BA1      (pin_G5)
#define MICROPY_HW_FMC_NBL0     (pin_E0)
#define MICROPY_HW_FMC_NBL1     (pin_E1)
#define MICROPY_HW_FMC_A0       (pin_F0)
#define MICROPY_HW_FMC_A1       (pin_F1)
#define MICROPY_HW_FMC_A2       (pin_F2)
#define MICROPY_HW_FMC_A3       (pin_F3)
#define MICROPY_HW_FMC_A4       (pin_F4)
#define MICROPY_HW_FMC_A5       (pin_F5)
#define MICROPY_HW_FMC_A6       (pin_F12)
#define MICROPY_HW_FMC_A7       (pin_F13)
#define MICROPY_HW_FMC_A8       (pin_F14)
#define MICROPY_HW_FMC_A9       (pin_F15)
#define MICROPY_HW_FMC_A10      (pin_G0)
#define MICROPY_HW_FMC_A11      (pin_G1)
#define MICROPY_HW_FMC_A12      (pin_G2)
#define MICROPY_HW_FMC_D0       (pin_D14)
#define MICROPY_HW_FMC_D1       (pin_D15)
#define MICROPY_HW_FMC_D2       (pin_D0)
#define MICROPY_HW_FMC_D3       (pin_D1)
#define MICROPY_HW_FMC_D4       (pin_E7)
#define MICROPY_HW_FMC_D5       (pin_E8)
#define MICROPY_HW_FMC_D6       (pin_E9)
#define MICROPY_HW_FMC_D7       (pin_E10)
#define MICROPY_HW_FMC_D8       (pin_E11)
#define MICROPY_HW_FMC_D9       (pin_E12)
#define MICROPY_HW_FMC_D10      (pin_E13)
#define MICROPY_HW_FMC_D11      (pin_E14)
#define MICROPY_HW_FMC_D12      (pin_E15)
#define MICROPY_HW_FMC_D13      (pin_D8)
#define MICROPY_HW_FMC_D14      (pin_D9)
#define MICROPY_HW_FMC_D15      (pin_D10)

//#define MICROPY_HW_QSPIFLASH_SIZE_BITS_LOG2
//#define MICROPY_HW_QSPIFLASH_CS		(pin_B6)
//#define MICROPY_HW_QSPIFLASH_SCK	(pin_B2)
//#define MICROPY_HW_QSPIFLASH_IO0	(pin_F8)
//#define MICROPY_HW_QSPIFLASH_IO1	(pin_F9)
//#define MICROPY_HW_QSPIFLASH_IO2	(pin_F7)
//#define MICROPY_HW_QSPIFLASH_IO3	(pin_F6)

#define MICROPY_HW_LTDC_WIDTH		(480)
#define MICROPY_HW_LTDC_HEIGHT		(272)

	/* Timing Configuration */
#define MICROPY_LTDC_TIMING_HSYNC	((uint16_t)41)
#define MICROPY_LTDC_TIMING_VSYNC	((uint16_t)10)
#define MICROPY_LTDC_TIMING_HBP		((uint16_t)2)
#define MICROPY_LTDC_TIMING_HFP		((uint16_t)2)
#define MICROPY_LTDC_TIMING_VBP		((uint16_t)2)
#define MICROPY_LTDC_TIMING_VFP		((uint16_t)2)

#define MICROPY_LTDC_HSPOLARITY		(LTDC_HSPOLARITY_AL)
#define MICROPY_LTDC_VSPOLARITY		(LTDC_VSPOLARITY_AL)
#define MICROPY_LTDC_DEPOLARITY		(LTDC_DEPOLARITY_AL)
#define MICROPY_LTDC_PCPOLARITY		(LTDC_PCPOLARITY_IPC)

#define MICROPY_LTDC_LAYER0_FB_ADDR	((uint32_t)0xD1F01000)
#define MICROPY_LTDC_LAYER0_FB_SIZE	(MICROPY_HW_LTDC_WIDTH*MICROPY_HW_LTDC_HEIGHT*4)
#define MICROPY_LTDC_LAYER1_FB_ADDR	((uint32_t)MICROPY_LTDC_LAYER0_FB_ADDR + MICROPY_LTDC_LAYER0_FB_SIZE)
#define MICROPY_LTDC_LAYER1_FB_SIZE	(MICROPY_HW_LTDC_WIDTH*MICROPY_HW_LTDC_HEIGHT*4)

#define MICROPY_HW_LCD_BL		(pin_I3)
#define MICROPY_HW_LTDC_CLK		(pin_G7)
#define MICROPY_HW_LTDC_HSYNC	(pin_I10)
#define MICROPY_HW_LTDC_VSYNC	(pin_I9)
#define MICROPY_HW_LTDC_DE		(pin_F10)
#define MICROPY_HW_LTDC_R7		(pin_G6)
#define MICROPY_HW_LTDC_R6		(pin_H12)
#define MICROPY_HW_LTDC_R5		(pin_H11)
#define MICROPY_HW_LTDC_R4		(pin_H10)
#define MICROPY_HW_LTDC_R3		(pin_H9)
#define MICROPY_HW_LTDC_R2		(pin_H8)
#define MICROPY_HW_LTDC_R1		(pin_H3)
#define MICROPY_HW_LTDC_R0		(pin_H2)
#define MICROPY_HW_LTDC_G7		(pin_I2)
#define MICROPY_HW_LTDC_G6		(pin_I1)
#define MICROPY_HW_LTDC_G5		(pin_I0)
#define MICROPY_HW_LTDC_G4		(pin_H15)
#define MICROPY_HW_LTDC_G3		(pin_H14)
#define MICROPY_HW_LTDC_G2		(pin_H13)
#define MICROPY_HW_LTDC_G1		(pin_E6)
#define MICROPY_HW_LTDC_G0		(pin_E5)
#define MICROPY_HW_LTDC_B7		(pin_I7)
#define MICROPY_HW_LTDC_B6		(pin_I6)
#define MICROPY_HW_LTDC_B5		(pin_I5)
#define MICROPY_HW_LTDC_B4		(pin_I4)
#define MICROPY_HW_LTDC_B3		(pin_G11)
#define MICROPY_HW_LTDC_B2		(pin_G10)
#define MICROPY_HW_LTDC_B1		(pin_G12)
#define MICROPY_HW_LTDC_B0		(pin_E4)
